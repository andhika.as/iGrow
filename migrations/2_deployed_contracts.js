
// var Basic = artifacts.require('./Basic.sol');

// module.exports = function(deployer) {
//     deployer.deploy(SimpleWallet);
// }

const iGrowCrowdsale = artifacts.require('./iGrowCrowdsale.sol');
const iGrowCoin = artifacts.require('./iGrowCoin.sol');

module.exports = function(deployer, network, accounts) {
    const openingTime = web3.eth.getBlock('latest').timestamp + 2; // two secs in the future
    const closingTime = openingTime + 86400 * 20; // 20 days
    const rate = new web3.BigNumber(1000);
    const wallet = accounts[1];

    return deployer
        .then(() => {
            return deployer.deploy(iGrowCoin);
        })
        .then(() => {
            return deployer.deploy(
                iGrowCrowdsale,
                openingTime,
                closingTime,
                rate,
                wallet,
                iGrowCoin.address
            );
        });
};