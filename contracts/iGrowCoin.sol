pragma solidity 0.4.19;

import 'zeppelin-solidity/contracts/token/MintableToken.sol';

contract iGrowCoin is MintableToken {
    string public name = "iGrow Coin";
    string public symbol = "IGC";
    uint8 public decimals = 20;
}